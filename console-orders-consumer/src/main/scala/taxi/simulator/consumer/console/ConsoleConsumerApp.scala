package taxi.simulator.consumer.console

import scala.concurrent.ExecutionContext.Implicits.global

object ConsoleConsumerApp {
  def main(args: Array[String]) {
    val consumer = ConsoleConsumer(ConsumerConf(topics = List("orders"), groupId = "group-console"))

    consumer.runWith { record =>
      println(s"Consumer record, offset = ${record.offset()}, value = ${record.value()}")
    }
  }
}