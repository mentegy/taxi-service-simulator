package taxi.simulator.consumer.console

import java.util.Properties
import java.util.concurrent.TimeoutException

import org.apache.kafka.clients.consumer.{ConsumerRecord, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.JavaConverters._
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try


class ConsoleConsumer(topics: List[String], conf: ConsumerConf) {
  private val log: Logger = LoggerFactory.getLogger(this.getClass)

  private val consumer = new KafkaConsumer[String, String](conf.toProperties,
    new StringDeserializer, new StringDeserializer)

  consumer.subscribe(topics.asJava)

  def runWith(fun: ConsumerRecord[String, String] => Unit)(implicit ec: ExecutionContext): Unit = {
    log.info(s"Running consumer groupId:${conf.groupId} connected to ${conf.servers}")
    log.info(s"Subscribed to topics ${topics.mkString("(", ", ", ")")}")
    run(fun)
  }

  private def run(fun: ConsumerRecord[String, String] => Unit)(implicit ec: ExecutionContext): Unit = {
    Try {
      Await.ready(Future(consumer.poll(1000).asScala.foreach(fun)), 30.seconds)
      run(fun)
    } recover {
      case e: TimeoutException =>
        log.error(s"Exceeded polling timeout, closing consumer", e)
        close()
    }
  }

  def close() = {
    log.info(s"Closing consumer with groupdId:${conf.groupId}")
    consumer.close()
  }

}

object ConsoleConsumer {
  def apply(conf: ConsumerConf): ConsoleConsumer = {
    new ConsoleConsumer(conf.topics, conf)
  }
}

case class ConsumerConf(topics: List[String], groupId: String, servers: String = "localhost:9092") {
  def toProperties: Properties = {
    val props = new Properties()
    props.put("bootstrap.servers", servers)
    props.put("group.id", groupId)
    props
  }
}
