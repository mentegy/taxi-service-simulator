package taxi.simulator.domain

import java.util.{Date, UUID}

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class District(name: String, streets: Vector[String])
object District extends DefaultJsonProtocol {
  implicit val districtFormat: RootJsonFormat[District] = jsonFormat2(District.apply)
}

case class Address(district: String, street: String, number: String)
object Address extends DefaultJsonProtocol {
  implicit val addressFormat: RootJsonFormat[Address] = jsonFormat3(Address.apply)
}

case class Order(uuid: String,
                 timestamp: Long,
                 phone: String,
                 destinations: List[Address],
                 downtime: Int, // in minutes
                 price: Option[BigDecimal],
                 assignee: Option[String]) {

  def withDestinations(dest: List[Address]) = Order(uuid, timestamp, phone, dest, downtime, None, assignee)
  def withDowntime(dest: List[Address]) = Order(uuid, timestamp, phone, dest, downtime, None, assignee)

  def withPrice(price: BigDecimal) = Order(uuid, timestamp, phone, destinations, downtime, Some(price), assignee)
}

object Order extends DefaultJsonProtocol {
  implicit val orderFormat: RootJsonFormat[Order] = jsonFormat(Order.apply, "uuid", "timestamp", "phone",
    "destinations", "downtime", "price", "assignee")

  def apply(phone: String, destinations: List[Address], downtime: Int = 0): Order =
    new Order(UUID.randomUUID.toString, new Date().getTime, phone, destinations, downtime, None, None)
}