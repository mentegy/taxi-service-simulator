package taxi.simulator.dispatchers

import spray.json.{DefaultJsonProtocol, JsArray, pimpString}
import taxi.simulator.domain.{Address, District, Order}

import scala.util.Random

class DummyOrderGenerator(districtFilePath: String) extends DefaultJsonProtocol {
  private val rand = new Random
  val districts: Vector[District] = parseDistricts(districtFilePath)

  def parseDistricts(filePath: String): Vector[District] = {
    val lines = scala.io.Source.fromInputStream(getClass.getResourceAsStream(filePath)).mkString

    lines.parseJson match {
      case arr: JsArray => arr.elements.map(_.convertTo[District])
      case _ => Vector.empty
    }
  }

  def randomAddress: Address = {
    val district = districts(rand.nextInt(districts.size))
    val street = district.streets(rand.nextInt(district.streets.size))

    Address(district.name, street, rand.nextInt(100).toString)
  }

  def randomOrder: Order = {
    val destinations = (1 to randomDestinationsCount).map(_ => randomAddress).toList
    val phone = "+380" + (1 to 9).map(_ => rand.nextInt(10)).mkString("")
    Order(phone, destinations)
  }

  def randomDestinationsCount: Int =
    Math.random() match {
      case x if x < 0.75 => 2
      case x if x < 0.9 => 3
      case _ => 4
    }
}
