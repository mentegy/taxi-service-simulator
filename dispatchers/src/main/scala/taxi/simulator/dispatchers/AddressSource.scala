package taxi.simulator.dispatchers

import akka.stream.scaladsl.Source
import akka.stream.stage.{GraphStage, GraphStageLogic, OutHandler}
import akka.stream.{Attributes, Outlet, SourceShape, ThrottleMode}
import spray.json.DefaultJsonProtocol
import taxi.simulator.domain.Order

import scala.concurrent.duration._
import scala.util.Random

class AddressSource(generator: DummyOrderGenerator) extends GraphStage[SourceShape[Order]] {
  val out: Outlet[Order] = Outlet("AddressSource")
  override val shape: SourceShape[Order] = SourceShape(out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) {
    setHandler(out, new OutHandler {
      override def onPull(): Unit = {
        push(out, generator.randomOrder)
      }
    })
  }
}

object AddressSource extends DefaultJsonProtocol {
  /* streams from 1 address to 30 per 1.5 second, randomly chosen */
  def source(districtsJson: String) = Source
    .fromGraph(new AddressSource(new DummyOrderGenerator(districtsJson)))
    .throttle(30, 1.second, 1, _ => new Random().nextInt(30) + 1, ThrottleMode.shaping)
}
