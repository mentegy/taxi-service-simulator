package taxi.simulator.dispatchers


import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream._
import spray.json.pimpAny
import taxi.simulator.domain.Order

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.language.implicitConversions

object DispatchersApp {
  def main(args: Array[String]): Unit = {

    implicit def addressToHttpEntity(order: Order): RequestEntity = HttpEntity(
      ContentTypes.`application/json`, order.toJson.compactPrint)

    implicit val system = ActorSystem("dispatchers-actor-system")
    implicit val materializer = ActorMaterializer()
    implicit val dispatcher = system.dispatcher

    val http = Http(system)
    val foreach: Future[Done] = AddressSource.source("/districts.json").runForeach { order =>
      println(s"Sending $order")
      http.singleRequest(HttpRequest(HttpMethods.POST, "http://localhost:8081/order", entity = order))
    }

    Await.result(system.whenTerminated, Duration.Inf)
  }
}
