package taxi.simulator.consumer.web

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.TextMessage
import akka.http.scaladsl.server.Directives
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import org.apache.kafka.common.serialization.StringDeserializer

import scala.concurrent.Await
import scala.concurrent.duration.Duration


object WebConsumerApp extends Directives {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()

    val consumerSettings = ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
      .withBootstrapServers("localhost:9092")
      .withGroupId("group-web")

    val map = Consumer
      .plainSource(consumerSettings, Subscriptions.topics("orders"))
      .map(record => TextMessage(record.value()))

    val ordersSource = map
    val route =
      pathSingleSlash {
        get {
          getFromResource("static/index.html")
        }
      } ~
      path("orders") {
        get {
          extractUpgradeToWebSocket { upgrade =>
            complete(upgrade.handleMessagesWithSinkSource(Sink.ignore, ordersSource))
          }
        }
      }

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    Await.ready(bindingFuture, Duration.Inf)
  }
}
