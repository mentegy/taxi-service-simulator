package taxi.simulator.kafka.embedded

import java.io.File
import java.net.InetSocketAddress

import org.apache.zookeeper.server.{ServerCnxnFactory, ZooKeeperServer}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try


class EmbeddedZooKeeper private(factory: ServerCnxnFactory, server: ZooKeeperServer,
                                snapshotDir: File, logDir: File)(implicit ec: ExecutionContext) extends Logging {

  val zkConnection = s"${factory.getLocalAddress.getHostName}:${factory.getLocalPort}"

  def startup(): Future[Unit] = Future {
    log.info(s"Starting embedded ZooKeeper instance on $zkConnection")
    factory.startup(server)
    log.info(s"Embedded ZooKeeper is started")
  } recover {
    case e => log.error(s"Failed to start embedded ZooKeeper due to: ${e.getMessage}", e)
  }

  def shutdown(): Future[Unit] = Future {
    log.info(s"Shutting down embedded ZooKeeper instance")
    factory.shutdown()
  } andThen {
    case _ =>
      log.debug(s"Deleting temporary created files")
      Utils.deleteTempDir(snapshotDir)
  } andThen {
    case _ => Utils.deleteTempDir(logDir)
  }

  def isRunning = server.isRunning
}

object EmbeddedZooKeeper {
  def apply(config: ZooKeeperConfig = ZooKeeperConfig())(implicit ec: ExecutionContext): Try[EmbeddedZooKeeper] =
    for {
      factory <- Try(ServerCnxnFactory.createFactory(new InetSocketAddress(config.interface, config.port),
        config.maxClientCnxns))
      snapshotDir <- Utils.buildTempDir(config.snapshotDirName)
      logDir <- Utils.buildTempDir(config.logDirName)
      server <- Try(new ZooKeeperServer(snapshotDir, logDir, config.tickTime))
    } yield new EmbeddedZooKeeper(factory, server, snapshotDir, logDir)
}

case class ZooKeeperConfig(interface: String = "localhost",
                           port: Int = Utils.findAvailablePort(),
                           snapshotDirName: String = "embeeded-zk/snapshot",
                           logDirName: String = "embeeded-zk/log",
                           tickTime: Int = 500,
                           maxClientCnxns: Int = 1024)
