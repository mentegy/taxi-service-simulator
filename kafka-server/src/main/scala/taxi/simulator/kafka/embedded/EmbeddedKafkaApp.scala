package taxi.simulator.kafka.embedded

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object EmbeddedKafkaApp extends Logging {
  def main(args: Array[String]): Unit = {
    EmbeddedZooKeeper(ZooKeeperConfig(port = 2181)).map { zooKeeper =>
      val kafkaCluster = EmbeddedKafkaCluster(List(9092), zooKeeper.zkConnection)

      sys.addShutdownHook {
        Await.ready(kafkaCluster.shutdown(), Duration.Inf)
        Await.ready(zooKeeper.shutdown(), Duration.Inf)
      }

      Await.ready(zooKeeper.startup().flatMap(_ => kafkaCluster.startup()), Duration.Inf)
    }
  }
}
