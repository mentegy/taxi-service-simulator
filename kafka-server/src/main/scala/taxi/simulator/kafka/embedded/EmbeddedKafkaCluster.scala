package taxi.simulator.kafka.embedded

import java.io.File
import java.util.Properties

import kafka.server.{KafkaConfig, KafkaServer}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class EmbeddedKafkaCluster private(_ports: List[Int], zkConnection: String)
                                  (implicit ec: ExecutionContext) extends Logging {
  private var brokers: List[KafkaServer] = List.empty
  private var logDirs: List[File] = List.empty
  private var _startedBrokers: String = _

  def startup(): Future[String] = Future {
    log.info(s"Starting embedded Kafka Cluster with ${_ports.size} brokers")
    _startedBrokers = _ports.zipWithIndex.map {
      case (port, index) => (Utils.buildTempDir("kafka-local").map { logDir =>
        val brokerId = s"${index + 1}"
        val server = newBroker(zkConnection, brokerId, s"$port", logDir.getAbsolutePath)
        server.startup()
        brokers ::= server
        logDirs ::= logDir
        log.info(s"Started Kafka broker $brokerId on port: $port")
        s"localhost:$port"
      } recover {
        case e: Throwable =>
          log.error(s"Failed to start broker on $port due to ${e.getMessage}", e)
          ""
      }).get
    }.mkString(",")
    log.info(s"Started Kafka cluster with brokers: $startedBrokers")
    startedBrokers
  }

  def startedBrokers: String = _startedBrokers

  def shutdown(): Future[List[Int]] = {
    log.info(s"Shutting down embedded Kafka cluster.")
    Future.sequence(brokers.map { broker =>
      Future {
        val id = broker.config.brokerId
        broker.shutdown()
        broker.awaitShutdown()
        log.info(s"Broker $id has been successfully stopped")
        id
      }
    }) andThen {
      case _ =>
        log.debug(s"Deleting temporary created files")
        logDirs.foreach(dir => Try(Utils.deleteTempDir(dir)))
    }
  }

  def ports: List[Int] = _ports

  private def newBroker(zkConn: String, brokerId: String, port: String, logDirPath: String): KafkaServer = {
    val props = new Properties
    props.put("zookeeper.connect", zkConn)
    props.put("broker.id", brokerId)
    props.put("host.name", "localhost")
    props.put("port", port)
    props.put("log.dir", logDirPath)
    props.put("log.flush.interval.messages", "1")
    new KafkaServer(KafkaConfig(props))
  }
}

object EmbeddedKafkaCluster {

  def apply(brokersAmount: Int, zkConnection: String)(implicit ec: ExecutionContext): EmbeddedKafkaCluster =
    new EmbeddedKafkaCluster(Utils.findAvailablePorts(brokersAmount), zkConnection)

  def apply(ports: List[Int], zkConnection: String)(implicit ec: ExecutionContext): EmbeddedKafkaCluster =
    new EmbeddedKafkaCluster(ports, zkConnection)
}
