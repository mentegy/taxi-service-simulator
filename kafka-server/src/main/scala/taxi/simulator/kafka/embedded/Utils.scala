package taxi.simulator.kafka.embedded

import java.io.File
import java.net.ServerSocket

import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Random, Try}

object Utils {
  def findAvailablePorts(size: Int): List[Int] = {
    require(size > 0, "must be greater than 0")

    val sockets = (1 to size).map(_ => new ServerSocket(0))
    val ports = sockets.map(_.getLocalPort).toList
    sockets.foreach(_.close())

    ports
  }

  def findAvailablePort(): Int = findAvailablePorts(1).head

  val random = new Random

  def buildTempDir(dirPrefix: String): Try[File] =
    Try(new File(System.getProperty("java.io.tmpdir"), dirPrefix + random.nextInt(1000000)))
      .map(file => (file, file.mkdirs()))
      .map {
        case (file, created) if created =>
          file.deleteOnExit()
          file
        case (file, _) => throw new RuntimeException("Could not create directory " + file.getAbsolutePath)
      }

  def deleteTempDir(dir: File): Boolean = {
    if (dir.exists && dir.isDirectory) {
      dir.listFiles().map(deleteTempDir).foldLeft(false)(_ && _)
    }
    else dir.delete()
  }
}

trait Logging {
  protected lazy val log: Logger = LoggerFactory.getLogger(this.getClass)
}
