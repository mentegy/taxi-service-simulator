package taxi.simulator.producer

import java.util.Properties

import org.apache.kafka.clients.producer._
import org.apache.kafka.common.serialization.StringSerializer


class Producer private(topic: String, props: Properties) {
  private val producer = new KafkaProducer[String, String](props, new StringSerializer, new StringSerializer)

  def send(message: String) = producer.send(new ProducerRecord[String, String](topic, message))

  def close() = producer.close()
}

object Producer {
  def apply(conf: ProducerConf): Producer = {
    val props = new Properties()
    props.put("bootstrap.servers", conf.servers)

    new Producer(conf.topic, props)
  }
}

case class ProducerConf(topic: String, servers: String = "localhost:9092")
