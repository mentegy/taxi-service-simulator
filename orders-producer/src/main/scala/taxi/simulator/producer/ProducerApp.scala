package taxi.simulator.producer

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object ProducerApp extends Directives {
  def main(args: Array[String]) {
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val producer = Producer(ProducerConf(topic = "orders"))

    val route = path("order") {
      post {
        entity(as[String]) { address =>
          complete {
            producer.send(address)
            "Ok"
          }
        }
      }
    }

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8081)
    println(s"Running http server on localhost:8081")

    Await.result(system.whenTerminated, Duration.Inf)
  }
}
