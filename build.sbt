import sbt.Keys._

resolvers in ThisBuild := Seq("Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/")

lazy val commonSettings = Seq(
  name in ThisBuild := "taxi-service-simulator",
  organization := "taxi.simulator",
  version := "0.1.0",
  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-encoding", "utf8"),
  scalaVersion := "2.11.8",
  libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"
)

lazy val `kafka-server` = (project in file("kafka-server"))
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
      "org.apache.kafka" %% "kafka" % kafkaVersion
        exclude("org.slf4j", "slf4j-log4j12")
        exclude("javax.jms", "jms")
        exclude("com.sun.jdmk", "jmxtools")
        exclude("com.sun.jmx", "jmxri"),
      "ch.qos.logback" % "logback-classic" % "1.1.7"
    ))

lazy val domain = (project in file("domain"))
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaVersion
    ))

lazy val dispatchers = (project in file("dispatchers"))
  .settings(commonSettings: _*)
  .settings(akkHttpDependencies: _*)
  .dependsOn(domain)

lazy val `orders-producer` = (project in file("orders-producer"))
  .settings(commonSettings: _*)
  .settings(akkHttpDependencies: _*)
  .settings(
    libraryDependencies ++= Seq(
      "org.apache.kafka" % "kafka-clients" % kafkaVersion
    ))
  .dependsOn(domain)

lazy val `console-orders-consumer` = (project in file("console-orders-consumer"))
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
      "org.apache.kafka" % "kafka-clients" % kafkaVersion
    ))
  .dependsOn(domain)

lazy val `web-orders-consumer` = (project in file("web-orders-consumer"))
  .settings(commonSettings: _*)
  .settings(akkHttpDependencies: _*)
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream-kafka" % akkaKafkaVersion
    ))
  .dependsOn(domain)

lazy val akkHttpDependencies = {
  libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-http-core" % akkaVersion,
    "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion
  )
}

exportJars := true

lazy val akkaVersion = "2.4.10"
lazy val kafkaVersion = "0.10.0.1"
lazy val akkaKafkaVersion = "0.12"